# -*- coding: utf-8 -*-
db.define_table('device_user_info',
                Field('device_id',requires=IS_NOT_EMPTY()),
                Field('device_name',requires=IS_NOT_EMPTY()),
                Field('location_places',requires=IS_NOT_EMPTY()),
                Field('device_model',requires=IS_NOT_EMPTY()),
                Field('device_type',requires=IS_NOT_EMPTY()),
                Field('status',requires=IS_NOT_EMPTY())
                )
